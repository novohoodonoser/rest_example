package ru.polushkin.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.polushkin.model.Measurement;
import ru.polushkin.model.api.MeasurementRepository;
import ru.polushkin.model.api.ModelConstants;
import ru.polushkin.rest.api.RestMeasurementsService;
import ru.polushkin.services.api.ValidateInputService;

import javax.validation.constraints.Size;
import java.time.DateTimeException;
import java.util.Date;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;


@RestController
public class MeasurementController {

    @Autowired
    RestMeasurementsService restMeasurementsService;

    /**
     * Title : Add measurement{@Link Measurement} instance into in-memory database
     * URL : /saveData
     * Method : POST
     * Data Params :
     *
     * @param gasUsage       [string] specify amount of used gas
     * @param hotWaterUsage  [string] specify amount of used hot water
     * @param coldWaterUsage [string] specify amount of used cold water
     *                       Response Codes: Success (200 OK), Bad Request (400), Unauthorized (401)
     */
    @RequestMapping(method = POST, value = "/saveData")
    public ResponseEntity saveMeasurements(@Size(min = 0, message = ModelConstants.MIN_GAS_USAGE_VALIDATION) @RequestParam(value = ModelConstants.GAS_USAGE) String gasUsage,
                                           @Size(min = 0, message = ModelConstants.MIN_HOT_WATER_USAGE_VALIDATION) @RequestParam(value = ModelConstants.HOT_WATER_USAGE) String hotWaterUsage,
                                           @Size(min = 0, message = ModelConstants.MIN_COLD_WATER_USAGE_VALIDATION) @RequestParam(value = ModelConstants.COLD_WATER_USAGE) String coldWaterUsage) {

        return restMeasurementsService.saveMeasurements(gasUsage, hotWaterUsage, coldWaterUsage);
    }

    /**
     * Title : Get measurements{@Link Measurement} instances in between two dates
     * URL : /findMeasurementsByDateOfMeasurementIsBetween
     * Method : GET
     * Data Params :
     *
     * @param startDate [string] beginning of the time interval input format yyyyMMdd
     * @param endDate   [string] ending of the time interval input format yyyyMMdd
     *                  Response Codes: Success (200 OK), Bad Request (400), Unauthorized (401)
     */
    @RequestMapping(method = GET, value = "/findMeasurementsByDateOfMeasurementIsBetween")
    public ResponseEntity findMeasurementsByDateOfMeasurementIsBetween(@RequestParam(value = ModelConstants.START_DATE) String startDate,
                                                                       @RequestParam(value = ModelConstants.END_DATE) String endDate) {

        return restMeasurementsService.findMeasurementsByDateOfMeasurementIsBetween(startDate, endDate);
    }
}
