package ru.polushkin.rest.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.polushkin.model.Measurement;
import ru.polushkin.model.api.MeasurementRepository;
import ru.polushkin.model.api.ModelConstants;
import ru.polushkin.rest.api.RestMeasurementsService;
import ru.polushkin.services.api.ValidateInputService;

import java.text.ParseException;
import java.time.DateTimeException;
import java.util.Date;
import java.util.Map;

@Service
public class RestMeasurementsServiceImpl implements RestMeasurementsService {


    @Autowired
    private MeasurementRepository repository;

    @Autowired
    ValidateInputService validateInputService;

    @Override
    public ResponseEntity saveMeasurements(String gasUsage, String hotWaterUsage, String coldWaterUsage) {
        Map<String, Long> data = validateInputService.validateAndRecieveMeasurementInputs(gasUsage, hotWaterUsage, coldWaterUsage);
        return new ResponseEntity(repository.save(new Measurement(
                data.get(ModelConstants.GAS_USAGE), data.get(ModelConstants.HOT_WATER_USAGE), data.get(ModelConstants.COLD_WATER_USAGE), new Date())), HttpStatus.OK);
    }

    @Override
    public ResponseEntity findMeasurementsByDateOfMeasurementIsBetween(String startDate, String endDate) {
        try {
            Map<String, Date> data = validateInputService.validateAndReceiveDateInputs(startDate, endDate);
            return new ResponseEntity(repository.findMeasurementsByDateOfMeasurementIsBetween(data.get(ModelConstants.START_DATE), data.get(ModelConstants.END_DATE)), HttpStatus.OK);
        } catch (DateTimeException ex) {
            return new ResponseEntity(ex.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (ParseException ex) {
            return new ResponseEntity(ModelConstants.WRONG_DATE_FORMAT_ERROR, HttpStatus.BAD_REQUEST);
        }
    }
}
