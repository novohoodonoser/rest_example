package ru.polushkin.rest.api;


import org.springframework.http.ResponseEntity;

public interface RestMeasurementsService {

    ResponseEntity saveMeasurements( String gasUsage, String hotWaterUsage, String coldWaterUsage);

    ResponseEntity findMeasurementsByDateOfMeasurementIsBetween( String startDate, String endDate);
}
