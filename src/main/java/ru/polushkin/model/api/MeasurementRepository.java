package ru.polushkin.model.api;

import org.springframework.data.repository.CrudRepository;
import ru.polushkin.model.Measurement;

import java.util.Date;
import java.util.List;


public interface MeasurementRepository extends CrudRepository<Measurement, Long> {

        List<Measurement> findByGasUsage(long gasUsage);

        List<Measurement> findMeasurementsByDateOfMeasurementIsBetween(Date start, Date end);
}