package ru.polushkin.model.api;


public class ModelConstants {
    //Measurements
    public static final String GAS_USAGE = "gasUsage";
    public static final String HOT_WATER_USAGE = "hotWaterUsage";
    public static final String COLD_WATER_USAGE = "coldWaterUsage";

    //Intervals
    public static final String START_DATE = "startDate";
    public static final String END_DATE = "endDate";
    public static final String DATE_PATTERN = "yyyyMMdd";


    //ValidationMessages
    public static final String MIN_GAS_USAGE_VALIDATION = "Gas Usage have to be more than zero";
    public static final String MIN_HOT_WATER_USAGE_VALIDATION = "Hot Water Usage have to be more than zero";
    public static final String MIN_COLD_WATER_USAGE_VALIDATION = "Cold Water Usage have to be more than zero";

    //Exceprions
    public static final String END_DATE_BEFORE_START_DATE_ERROR = "startDate has to be before endDate";
    public static final String WRONG_DATE_FORMAT_ERROR = "Please enter dates in yyyyMMdd format";

}
