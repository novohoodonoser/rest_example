package ru.polushkin.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.util.Date;

@Entity
public class Measurement {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private long gasUsage;

    private long hotWaterUsage;

    private long coldWaterUsage;

    private Date dateOfMeasurement;

    public Measurement() {
    }

    public Measurement(long id, long gasUsage, long hotWaterUsage, long coldWaterUsage, Date dateOfMeasurement ) {
        this.id = id;
        this.gasUsage = gasUsage;
        this.hotWaterUsage = hotWaterUsage;
        this.coldWaterUsage = coldWaterUsage;
        this.dateOfMeasurement = dateOfMeasurement;
    }

    public Measurement( Long gasUsage, Long hotWaterUsage, Long coldWaterUsage, Date dateOfMeasurement ) {
        this.gasUsage = gasUsage;
        this.hotWaterUsage = hotWaterUsage;
        this.coldWaterUsage = coldWaterUsage;
        this.dateOfMeasurement = dateOfMeasurement;
    }

    public long getGasUsage() {
        return gasUsage;
    }

    public long getHotWaterUsage() {
        return hotWaterUsage;
    }

    public long getColdWaterUsage() {
        return coldWaterUsage;
    }

    public Date getDateOfMeasurement() {
        return dateOfMeasurement;
    }

    @Override
    public String toString() {
        return "Measurement{" +
                "id=" + id +
                ", gasUsage=" + gasUsage +
                ", hotWaterUsage=" + hotWaterUsage +
                ", coldWaterUsage=" + coldWaterUsage +
                ", dateOfMeasurement=" + dateOfMeasurement +
                '}';
    }
}
