package ru.polushkin.services.api;

import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;


public interface ValidateInputService
{
    Map<String, Long> validateAndRecieveMeasurementInputs(String gasUsage, String hotWaterUsage, String coldWaterUsage);

    Map<String,Date> validateAndReceiveDateInputs(String startDate, String endDate) throws ParseException;
}
