package ru.polushkin.services.impl;


import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.polushkin.model.api.ModelConstants;
import ru.polushkin.services.api.ValidateInputService;
import java.text.ParseException;
import java.time.DateTimeException;
import java.util.Date;
import java.util.Map;

@Service
public class ValidateInputServiceImpl implements ValidateInputService {

    public Map<String, Long> validateAndRecieveMeasurementInputs(String gasUsage, String hotWaterUsage, String coldWaterUsage) {
        Long lGasUsage = Long.valueOf(gasUsage);
        Long lHotWaterUsage = Long.valueOf(hotWaterUsage);
        Long lColdWaterUsage = Long.valueOf(coldWaterUsage);
        return ImmutableMap.<String, Long>builder()
                .put(ModelConstants.GAS_USAGE, lGasUsage)
                .put(ModelConstants.HOT_WATER_USAGE, lHotWaterUsage)
                .put(ModelConstants.COLD_WATER_USAGE, lColdWaterUsage)
                .build();
    }

    @Override
    public Map<String, Date> validateAndReceiveDateInputs(String startDate, String endDate) throws ParseException {
        Date dStartDate = DateUtils.parseDate(startDate, ModelConstants.DATE_PATTERN);
        Date dEndDate = DateUtils.parseDate(endDate, ModelConstants.DATE_PATTERN);
        if (dEndDate.before(dStartDate)) {
            throw new DateTimeException(ModelConstants.END_DATE_BEFORE_START_DATE_ERROR);
        }
        return ImmutableMap.<String, Date>builder()
                .put(ModelConstants.START_DATE, dStartDate)
                .put(ModelConstants.END_DATE, dEndDate)
                .build();
    }
}
