package ru.polushkin;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import ru.polushkin.model.api.MeasurementRepository;
import ru.polushkin.model.Measurement;

import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.Date;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses=MeasurementRepository.class)
public class MainApplication {
    private static final Logger log = LoggerFactory.getLogger(MainApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(MeasurementRepository measurementRepository) {
        return (args) -> {
            // save a couple of measurements
            measurementRepository.save(new Measurement(1,2,3,4,new Date()));
            measurementRepository.save(new Measurement(2,3,4,5,new Date()));
            LocalDate date1 = LocalDate.of(2012, Month.DECEMBER, 12);
            LocalDate date2 =  LocalDate.of(2012, Month.DECEMBER, 13);
            LocalDate date3 =  LocalDate.of(2012, Month.DECEMBER, 14);
            measurementRepository.save(new Measurement(3,3,4,5,Date.from(date1.atStartOfDay(ZoneId.systemDefault()).toInstant())));
            measurementRepository.save(new Measurement(4,3,4,5,Date.from(date2.atStartOfDay(ZoneId.systemDefault()).toInstant())));
            measurementRepository.save(new Measurement(5,3,4,5,Date.from(date3.atStartOfDay(ZoneId.systemDefault()).toInstant())));

            // fetch all measurements
            log.info("Measurement found with findAll():");
            log.info("-------------------------------");
            for (Measurement measurement : measurementRepository.findAll()) {
                log.info(measurement.toString());
            }
            log.info("");
        };
    }
}
