Simple REST API for saving Measurements entitys in in-memory db by calling /saveData 
and recieveng some history in provided date interval by calling /findMeasurementsByDateOfMeasurementIsBetween.
The application starts from main method in ru.polushkin.MainApplication
Creating test data appears in ru.polushkin.MainApplication#demo.
After that we can start interact with our api
Implements some logical checks like checking endDate has to be greater than startDate
Calls can be send via Postman app
The application implements basic authentication for one user username "depo" password "pas" 
Unit Tests exists in ru.polushkin.services.MeasurementControllerTest

